package org.filestorage.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@NoArgsConstructor
public class AuthorizationRequest {

    @NotBlank
    private String login;
    private String password;

}
