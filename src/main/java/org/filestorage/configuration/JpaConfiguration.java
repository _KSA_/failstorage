package org.filestorage.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationVersion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
public class JpaConfiguration {

    @Value("${filestorage.db.host}")
    private String host;
    @Value("${filestorage.db.name}")
    private String dbName;
    @Value("${filestorage.db.port}")
    private String port;
    @Value("${filestorage.db.username}")
    private String username;
    @Value("${filestorage.db.password}")
    private String password;
    @Value("${filestorage.db.version}")
    private String version;

    @Bean
    public DataSource dateSource() {
        System.out.println("Start server");
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/%s", host, port, dbName));
        return dataSource;
    }

    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource){
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setTarget(MigrationVersion.fromVersion(version));
        return flyway;
    }
}
