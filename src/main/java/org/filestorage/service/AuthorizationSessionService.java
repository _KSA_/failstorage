package org.filestorage.service;


        import org.filestorage.domain.dto.UserSession;

public interface AuthorizationSessionService {

    UserSession createOrUpdateSession(String login);

    boolean isExpired(String sid);
}
