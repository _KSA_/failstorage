package org.filestorage.service;

public interface AuthenticationService {

    boolean authenticate(String login, String password);
}
