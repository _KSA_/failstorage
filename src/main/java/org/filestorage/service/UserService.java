package org.filestorage.service;

import org.filestorage.domain.dto.User;

import java.util.Collection;

public interface UserService {
    boolean auth(String login, String password);

    Collection<User> getAll();
}
