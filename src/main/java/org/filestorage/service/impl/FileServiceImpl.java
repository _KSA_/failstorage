package org.filestorage.service.impl;

import org.filestorage.domain.dto.File;
import org.filestorage.domain.entity.FileEntity;
import org.filestorage.repository.FileRepository;
import org.filestorage.service.FileService;
import org.modelmapper.ModelMapper;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FileServiceImpl implements FileService {

    private  final FileRepository fileRepository;
    private  final ModelMapper modelMapper;

    public FileServiceImpl(FileRepository filesRepository, ModelMapper modelMapper) {
        this.fileRepository = filesRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<File> getAll() {
        Iterable<FileEntity> iterable = fileRepository.findAll();
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(entity -> modelMapper.map(entity, File.class))
                .collect(Collectors.toList());
    }
}
