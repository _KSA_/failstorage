package org.filestorage.service.impl;


import org.filestorage.domain.dto.User;
import org.filestorage.domain.entity.UserEntity;
import org.filestorage.repository.UserRepository;
import org.filestorage.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public boolean auth(String login, String password) {
        return false;
    }

    @Override
    public Collection<User> getAll() {
        Iterable<UserEntity> iterable = userRepository.findAll();
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(entity -> modelMapper.map(entity, User.class))
                .collect(Collectors.toList());
    }
}
