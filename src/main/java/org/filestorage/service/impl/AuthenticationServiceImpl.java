package org.filestorage.service.impl;


import org.filestorage.domain.entity.UserEntity;
import org.filestorage.repository.UserRepository;
import org.filestorage.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean authenticate(String login, String password) {
        UserEntity entity = userRepository.findByLogin(login);
        return entity !=null && entity.getPassword().equals(password);
    }
}
