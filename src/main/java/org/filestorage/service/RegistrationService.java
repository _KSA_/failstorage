package org.filestorage.service;

import org.filestorage.domain.dto.RegistrationRequest;

public interface RegistrationService {
    void register(RegistrationRequest request);
}
