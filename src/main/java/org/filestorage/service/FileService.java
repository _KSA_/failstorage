package org.filestorage.service;

import org.filestorage.domain.dto.File;
import java.util.Collection;

public interface FileService {
    Collection<File> getAll();
}
