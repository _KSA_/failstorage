package org.filestorage.repository;

import org.filestorage.domain.entity.FileEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository <FileEntity, Integer> {
    FileEntity findByFilesName(String login);
}
