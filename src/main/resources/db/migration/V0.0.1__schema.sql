create table if not exists users(
       id serial primary key,
       login varchar(30) not null unique,
       password varchar(100) not null
);
create table if not exists files (
       id serial primary key,
  	   user_Id INTEGER,
       file_name varchar(200) not null unique,
  	FOREIGN KEY (user_Id) REFERENCES users (Id)
);

insert into users (login, password)
  values
        ('user', 'pass'),
        ('admin', '1');

 insert into files (user_Id, file_name)
 values
        ('1', 'userFiles1'),
        ('2', 'adminFiles1'),
        ('1', 'userFiles2'),
        ('2', 'adminFiles2'),
        ('1', 'userFiles3'),
        ('2', 'adminFiles3'),
        ('1', 'userFiles4'),
        ('2', 'adminFiles4'),
        ('1', 'userFiles5'),
        ('2', 'adminFiles5');